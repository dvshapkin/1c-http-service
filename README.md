### Это описание того, как создать Docker-контейнер с web-сервером Apache, настроенным для работы с http-службами из 1С.


#### Сначала описание Dockerfile
```Dockerfile
FROM httpd:latest

WORKDIR /usr/local/apache2
COPY src src
COPY httpd.conf conf
COPY default.vrd htdocs
RUN dpkg -i src/1c*.deb

```

Создаем образ на базе официального **httpd**.
Каталог **src** (источник) должен содержать следующие файлы:
- 1c-enterprise83-ws_8.3.xx-xxxx_amd64.deb
- 1c-enterprise83-common_8.3.xx-xxxx_amd64.deb
- 1c-enterprise83-server_8.3.xx-xxxx_amd64.deb

Собственно, нужен только первый пакет, но он зависит от второго и не работает без третьего :)

Файл **httpd.conf** должен быть предварительно настроен, а именно, нужно добавить в него следующие секции:
```
# 1c publication
Alias "/ut-dsh-long" "/usr/local/apache2/htdocs/"
<Directory "/usr/local/apache2/htdocs/">
    AllowOverride All
    Options None
    Require all granted
    SetHandler 1c-application
    ManagedApplicationDescriptor "/usr/local/apache2/htdocs/default.vrd"
</Directory>


LoadModule _1cws_module /opt/1C/v8.3/x86_64/wsap24.so
```

Файл **default.vrd** должен иметь следующее содержание:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<point xmlns="http://v8.1c.ru/8.2/virtual-resource-system"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		base="/ut-dsh-long"
		ib="Srvr=&quot;192.168.1.216&quot;;Ref=&quot;ut-dsh-long&quot;;Usr=&quot;web&quot;;"
		enable="false">
	<ws enable="false"/>
	<httpServices>
		<service name="ServicesUT"
				rootUrl="ut"
				enable="true"
				reuseSessions="autouse"
				sessionMaxAge="20"
				poolSize="10"
				poolTimeout="5"/>
		<service name="ServicesRER"
				rootUrl="rer"
				enable="true"
				reuseSessions="autouse"
				sessionMaxAge="20"
				poolSize="10"
				poolTimeout="5"/>
	</httpServices>
</point>
```

#### Сборка образа
`docker build -t dvshapkin/apache .`

#### Запуск контейнера
`docker run --name apache -p 8080:80 -dti dvshapkin/apache`

#### Доступ к службе 1с по url
`localhost:8080/ut-dsh-long/hs/ut/hello`
