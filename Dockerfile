FROM httpd:latest

WORKDIR /usr/local/apache2
COPY src src
COPY httpd.conf conf
COPY default.vrd htdocs
RUN dpkg -i src/1c*.deb
